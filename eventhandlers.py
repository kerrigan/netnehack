# -*- coding: utf-8 -*-
import math
import random


from constants import *
from objects import Monster, Weapon
from utils import call_later, randweight, d
from game_utils import generate_trophy

empty_cb = lambda user, msg, progress: None


def walk(user, progress=0, callback=empty_cb):
    callback(user, "Герой идет по подземелью", progress)
    if progress == 100:
        before = math.floor(user.hp)
        user.heal(random.random() * 3)
        if math.floor(user.hp) > before:
            message = "Герой восстанавливает здоровье до " + str(int(math.floor(user.hp)))
            callback(user, message, progress)
        return create_event(user, callback)
    elif progress == 0:
        # message = "Герой идет по подземелью"
        # callback(user, message, progress)
        progress += 50
        return call_later(2, walk, user, progress, callback)
    else:

        progress += 50
        return call_later(2, walk, user, progress, callback)


def fight_monster(user, monster=None, progress=0, callback=empty_cb):
    hp_progress = lambda hp, maxhp: int(round(1 - (float(hp) / maxhp), 2) * 100)
    ATTACK_FROM = 0
    ATTACK_TO = 1
    if monster is None:
        monster_data = random.choice(MONSTERS)
        monster = Monster(monster_data['level'], monster_data['name'],
                          monster_data['attack'])
        message = monster.name + " вступает в бой с героем"
        next_call = call_later(2, fight_monster, user, monster, callback=callback)
        user.current = next_call
        callback(user, message, progress)
        return next_call
    else:
        result = random.choice((ATTACK_TO, ATTACK_FROM))
        if result == ATTACK_FROM:
            damage_modifier = user.weapon.get_damage_bonus()
            damage = user.weapon.get_damage() * damage_modifier
            if damage > 0:
                monster.hp -= damage
                if damage_modifier == 1:
                    message = "Герой атакует " + monster.name + " на " + str(damage) + " единиц урона"
                elif damage_modifier > 1:
                    message = "Герой наносит критический удар " + monster.name + " на " + str(damage) + " урона"
            else:
                message = "Герой промахивается"
        elif result == ATTACK_TO:
            damage = d(*monster.attack)
            modifier = random.randint(1, 20)
            if modifier > 1:
                message = monster.name + " атакует героя на " + str(damage) + " единиц урона"
            else:
                message = monster.name + " промахивается"
            user.hp -= damage
        callback(user, message, progress)

        if user.hp <= 0:
            return user_death(user, callback=callback)
        elif monster.hp <= 0:
            return call_later(2, user_kill_monster, user, monster, callback=callback)
        elif user.hp <= 0.1 * user.maxhp:
            return call_later(2, run_away, user, monster, callback=callback)
        else:
            next_call = call_later(2, fight_monster, user, monster,
                                   hp_progress(monster.hp, monster.maxhp),
                                   callback=callback)
            user.current = next_call
            return next_call


def user_kill_monster(user, monster, progress=100, callback=empty_cb):
    message = "Герой победил " + monster.name
    user.earn_exp(random.random() * 0.1)
    user.monsters += 1
    callback(user, message, progress)
    next_call = call_later(2, get_trophy, user, callback=callback)
    user.current = next_call
    return next_call


def get_trophy(user, progress=100, callback=empty_cb):
    from operator import mul
    if random.random() <= 0.2:
        trophy = generate_trophy()
        if isinstance(trophy, Weapon):
            message = "Герой нашел " + trophy.name
            callback(user, message, progress)
            if reduce(mul, trophy.damage) > reduce(mul, user.weapon.damage):
                message = "Герой поменял свое оружие на " + trophy.name
                user.weapon = trophy
                callback(user, message, progress)
    else:
        message = "Герой обыскал труп монстра, но ничего не нашел"
        callback(user, message, progress)
    next_call = create_event(user, callback)
    return next_call


def run_away(user, monster, progress=100, callback=empty_cb):
    message = "Оставшись с %f единицами здоровья, герой сбегает от %s" % (user.hp, monster.name)
    callback(user, message, progress)
    return call_later(2, walk, user, callback=callback)


def user_death(user, progress=100, callback=empty_cb):
    message = "Герой погиб, победив %s монстров" % user.monsters
    next_call = call_later(10, user_resurrection, user, callback=callback)
    user.current = next_call
    callback(user, message, progress)
    return next_call
    #reactor.stop()


def user_resurrection(user, progress=100, callback=empty_cb):
    lose_exp = user.exp * 0.1
    message = "Герой воскрешен с потерей %s%% опыта" % (lose_exp * 100)
    user.exp -= lose_exp
    user.hp = user.maxhp
    callback(user, message, progress)
    next_call = create_event(user, callback)
    user.current = next_call
    return next_call


EVENTS = {WALK: walk,
          FIGHT_MONSTER: fight_monster,
          }

EVENT_PROBS = {80: WALK,
               20: FIGHT_MONSTER}


def create_event(user, callback, events=None):
    if events is None:
        events = EVENT_PROBS
    if user.hp <= 0:
        event_func = user_resurrection
    else:
        event = randweight(events)
        event_func = EVENTS[event]
    next_call = call_later(2, event_func, user, callback=callback)
    user.current = next_call
    return next_call
