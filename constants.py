# -*- coding: utf-8 -*-
# CONFIG
OLD_SESSIONS_CHECK = 120
SESSION_TIMEOUT = 7200
# EVENTS
WALK = 0
FIGHT_MONSTER = 1
RUN_AWAY = 2
MONSTER_DEATH = 3
USER_DEATH = 4
USER_RESURRECTION = 5


# WEAPON
SMALL = 0
MEDIUM = 1

import csv

MONSTERS = []
if MONSTERS == []:
    MONSTERS = [{'name': monster[0], 'level': int(monster[1]),
                 'attack': (int(monster[2]), int(monster[3]))} for monster in
                csv.reader(open("monsters.txt"), delimiter="|")]
    
WEAPONS = []
if WEAPONS == []:
    WEAPONS = [{'name': line['Weapon'], 'damageS': map(int, line['DmgS'].split("d")),
                'damageM': map(int, line['DmgM'].split("d")),
                'crit': int(line['Critical'])}
               for line in csv.DictReader(open("weapons.csv"), delimiter="|")]
