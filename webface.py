from flask import Flask, render_template, g, session, request, make_response
from gevent.wsgi import WSGIServer


app = Flask(__name__)

@app.route("/")
def main():
    response = make_response(render_template("wsclient.html"))
    response.set_cookie('sessionid', 1)
    return response

if __name__ == '__main__':
    app.run(debug=True)
