# -*- coding: utf-8 -*-
from eventhandlers import call_later, create_event

from objects import User, Weapon

USERS = {1: {'id': 1, 'hp': 15, 'mp': 10,
             'maxhp': 15, 'maxmp': 10,
             'level': 1, 'exp': 0, 'monsters': 0,
             "weapon": {"name": "Unarmed", "size": 0,
                        "damage": (1, 4), "crit": 2}
             }}
SESSIONS = {"1": 1}


def get_user(sessionid):
    if sessionid in SESSIONS:
        data = USERS[SESSIONS[sessionid]]
        weapon_data = data['weapon']
        weapon = Weapon(weapon_data['name'],
                        weapon_data['size'],
                        weapon_data['damage'],
                        weapon_data['crit'])
        user = User(data['id'],
                    data['hp'], data['mp'],
                    weapon,
                    data['maxhp'], data['maxmp'],
                    data['level'], data['exp'],
                    data['monsters'],)
        # if 'current' in data:
        #     func, args, kwargs = data['current']
        #     user.current = call_later(2, func, *args, **kwargs)
        return user
    raise Exception

def save_user(user):
    USERS.update({user.id: user.to_dict()})
    # user.current.reset(50)
    # functuple = (user.current.func, )
    # if hasattr(user.current, "args"):
    #     functuple += (user.current.args, )
    # else:
    #     functuple += ((), )
    # if hasattr(user.current, "kwargs"):
    #     functuple += (user.current.kwargs, )
    # else:
    #     functuple += ({}, )
    # USERS[user.id]['current'] = functuple
    # user.current.cancel()
