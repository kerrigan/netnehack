# -*- coding: utf-8 -*-
import random
import math
import json
import uuid

from constants import *
from utils import d, randexp

class Weapon(object):
    """
    """
    size = SMALL
    damage = 0, 2
    critical = 1
    name = "Unarmed"
    
    def __init__(self, name, size, damage, critical):
        self.name = name
        self.damage = damage
        self.size = size
        self.critical = critical

    def get_damage_bonus(self):
        attack_roll = d(1, 20)
        if attack_roll == 1:
            return 0
        elif 1 < attack_roll < 20:
            return 1
        else:
            return self.critical

    def get_damage(self):
        return d(*self.damage)

    def to_dict(self):
        return {'name': self.name,
                'size': self.size,
                'damage': self.damage,
                'crit': self.critical}

    def to_json(self):
        return json.dumps(self.to_dict())



class User(object):
    current = None
    current_args = {}
    spells = set()
    monsters = 0
    level = 1
    exp = 0
    weapon = Weapon("Unarmed", SMALL, (1, 2), 2)

    def __init__(self, uid, hp, mp, weapon, maxhp=None, maxmp=None, level=1, exp=0, monsters=0):
        self.id = uid
        self.maxhp = maxhp or hp
        self.hp = hp
        self.maxmp = maxmp or mp
        self.mp = mp
        self.level = level
        self.exp = exp
        self.monsters = monsters
        self.weapon = weapon
        self.godpower = 100

    def heal(self, hps):
        if self.hp + hps <= self.maxhp:
            self.hp += hps
        else:
            self.hp = self.maxhp

    def earn_exp(self, exp):
        """
        Получить опыт, при победе монстра, например
        """
        new_exp = self.exp + exp
        if new_exp > 1:
            self.level_up()
            self.exp = new_exp - 1
        else:
            self.exp += exp

    def lose_exp(self, exp):
        """
        Потерять опыт при гибели
        """
        if self.exp > 0:
            self.exp -= exp

    def level_up(self):
        """
        Повышение уровня. Здесь логика поднятия характеристик
        """
        self.level += 1
        self.maxhp += 2
        self.hp = self.maxhp

    def to_json(self):
        return json.dumps(self.to_dict())

    def to_dict(self):
        return {
            "id": self.id,
            "hp": self.hp > 0 and math.floor(self.hp) or 0,
            "maxhp": self.maxhp,
            "mp": math.floor(self.mp),
            "maxmp" :self.maxmp,
            "level": self.level,
            "exp": self.exp,
            "monsters": self.monsters,
            "weapon": self.weapon.to_dict(),
            }



class Monster(object):

    def __init__(self, level, name, attack):
        assert(isinstance(attack, tuple))
        self.level = level
        hp = d(level, 8)
        self.hp = hp
        self.maxhp = hp
        self.name = name
        self.attack = attack
