# -*- coding: utf-8 -*-
from twisted.internet import reactor

from objects import User
from eventhandlers import create_event




def mainloop(callback=lambda user, msg: None):
    wanderer = User(15, 10)
    return create_event(wanderer, callback=callback)
    
if __name__ == '__main__':
    mainloop()
    reactor.run()
