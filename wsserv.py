# -*- coding: utf-8 -*-
import datetime
import json

from twisted.internet import reactor, task
from twisted.python import log

from autobahn.websocket import WebSocketServerFactory, WebSocketServerProtocol, listenWS

from constants import OLD_SESSIONS_CHECK, SESSION_TIMEOUT
from eventhandlers import create_event
import datastore


class DungeonServerProtocol(WebSocketServerProtocol):
    user = None

    def sendMessage(self, message, binary):
        WebSocketServerProtocol.sendMessage(self, message, binary)

    def send_event(self, user, message, progress=0):
        now = datetime.datetime.now()
        jsmsg = {"timestamp": now.strftime("%d-%m-%Y %H:%M:%S"),
                 "message": message,
                 "user": user.to_json(),
                 "progress": progress}
        self.sendMessage(json.dumps(jsmsg), False)

    def onOpen(self):
        if 'cookie' in self.http_headers:
            cookies = dict([i.split("=", 1) for i in self.http_headers['cookie'].split("; ")])
            # получаем кукис с данными о сессии
            sessionid = cookies.get("sessionid")
            self.user = datastore.get_user(sessionid)
            if self.user.id in self.factory.sessions:
                self.user = self.factory.sessions[self.user.id]['user']
            else:
                self.user.current = create_event(self.user, self.factory.broadcast)
            self.factory.register(self)
        else:
            """
            Рвем соединение, если нет сессии пользователя
            """
            self.transport.abortConnection()

    def onMessage(self, msg, binary):
        self.factory.broadcast(self.user, "Пиздюль отправляется с неба герою...", 100)
        if self.user.current.active():
            self.user.current.reset(0)
        else:
            """
            Если глас уже послан, но не действие уже выполнено
            """
            pass

    def onClose(self, wasClean, code, reason):
        log.msg("connection closed")
        if self.user and self.user.id in self.factory.sessions:
            self.factory.unregister(self)


class DungeonServerFactory(WebSocketServerFactory):

    protocol = DungeonServerProtocol

    def __init__(self, url):
        WebSocketServerFactory.__init__(self, url)
        task.LoopingCall(self.close_old_sessions).start(OLD_SESSIONS_CHECK,
                                                        now=False)
        self.sessions = {}
        self.tickcount = 0

    def close_old_sessions(self):
        old_sessions = {userid: session for userid, session in self.sessions.iteritems()
                        if ("logout_time" in session) and\
                            (datetime.datetime.now() - session["logout_time"]).seconds >= SESSION_TIMEOUT}
        for userid, session in old_sessions.iteritems():
            current = session['user'].current
            if current.active():
                current.cancel()
                datastore.save_user(session['user'])
                del self.sessions[userid]
                log.msg("Session finished for %s" % session['user'].id)

    def register(self, client):
        if not client.user.id in self.sessions:
            log.msg("registered client " + client.peerstr)
            self.sessions[client.user.id] = {"user": client.user, "sessions": [client]}
        else:
            self.sessions[client.user.id]["sessions"].append(client)
        if "logout_time" in self.sessions[client.user.id]:
            del self.sessions[client.user.id]["logout_time"]

    def unregister(self, client):
        if client.user.id in self.sessions:
            # print "client", client.peerstr, "out"
            self.sessions[client.user.id]["sessions"].remove(client)
            if len(self.sessions[client.user.id]["sessions"]) == 0:
                self.sessions[client.user.id]["logout_time"] = datetime.datetime.now()

    def broadcast(self, user, message, progress):
        log.msg("message: " + message + " " + str(progress) + "%")
        if user.id in self.sessions:
            for client in self.sessions[user.id]['sessions']:
                client.send_event(user, message, progress)


factory = DungeonServerFactory("ws://localhost:9004")
factory.protocol = DungeonServerProtocol

"""
Web interface code
"""
from twisted.web.wsgi import WSGIResource
from twisted.web.server import Site
from webface import app
resource = WSGIResource(reactor, reactor.getThreadPool(), app)
site = Site(resource)

if __name__ == '__main__':
    import sys
    log.startLogging(sys.stdout)
    listenWS(factory)
    reactor.listenTCP(5000, site)
    reactor.run()
else:
    from twisted.application import internet, service
    wsserver = internet.TCPServer(9004, factory)
    webiface = internet.TCPServer(5000, site)
    application = service.Application("DungeonServer")
    wsserver.setServiceParent(application)
    webiface.setServiceParent(application)
