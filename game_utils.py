# -*- coding: utf-8 -*-
import random

from constants import WEAPONS, SMALL, MEDIUM
from objects import Weapon


def generate_trophy(types=None):
    weapon_data = random.choice(WEAPONS)
    weapon_size = random.choice((SMALL, MEDIUM))
    weapon_crit = weapon_data['crit']
    weapon_damage = weapon_size == SMALL and\
        weapon_data['damageS'] or weapon_data['damageM']
    weapon = Weapon(weapon_data['name'], weapon_size,
                    weapon_damage, weapon_crit)
    return weapon
