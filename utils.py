# -*- coding: utf-8 -*-
import cmath
import random
from twisted.internet import reactor


def call_later(timeout, event, *args, **kwargs):
    return reactor.callLater(timeout, event, *args, **kwargs)

def randexp(mid):
    return mid * cmath.log(1 / random.random()).real

def randweight(weights):
    assert(isinstance(weights, dict))
    total = 0
    winner = 0
    for i, w in weights.iteritems():
        total += i
        if random.random() * total < i:
            winner = w
    return winner

def d(n, x):
    result = 0
    if n >= 1:
        for i in xrange(n):
            result += random.randint(1, x)
    else:
        result = n * random.randint(1, x)
    return result


    
